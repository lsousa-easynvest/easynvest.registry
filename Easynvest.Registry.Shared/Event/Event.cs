﻿using System;

namespace Easynvest.Registry.Shared.Event
{
    public abstract class Event
    {
        protected Event()
        {
            Type = this.GetType().Name;
            this.Ocurrency = DateTime.Now;
        }

        public string Type { get; private set; }
        public DateTime Ocurrency { get; private set; } 
    }
}
