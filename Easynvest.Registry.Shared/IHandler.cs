﻿using Easynvest.Registry.Shared.Command;

namespace Easynvest.Registry.Shared
{
    public interface IHandler<in T> where T : ICommand
    {
        void Handle(T command);
    }
}
