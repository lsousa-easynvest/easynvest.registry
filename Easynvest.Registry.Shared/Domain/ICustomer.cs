﻿namespace Easynvest.Registry.Shared.Domain
{
    public interface ICustomer
    {
        Identity<string> Id { get; set; }
    }
}
