﻿using System;
using System.Collections.Generic;

namespace Easynvest.Registry.Shared.Domain
{
    public sealed class Identity<T> : IEquatable<Identity<T>>
    {
        private T _value;

        public Identity()
        {
            
        }

        public Identity(T value)
        {
            this.Value = value;
        }

        public T Value
        {
            get { return _value; }
            set
            {
                if(value == null) throw new ArgumentNullException();

                _value = value;
            }
        }

        public static Identity<T> Create(T value)
        {
            return new Identity<T>(value);
        }

        public override bool Equals(object obj)
        {
            var other = obj as Identity<T>;

            return Equals(other);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public bool Equals(Identity<T> other)
        {
            if (other == null) return false;
            if (!object.ReferenceEquals(other, this)) return false;

            return EqualityComparer<T>.Default.Equals(other.Value, Value);
        }

        public override string ToString()
        {
            return $"Value: {Value}";
        }
    }
}
