﻿namespace Easynvest.Registry.Shared.Domain
{
    public abstract class EntityWithId<T> : Entity
    {
        public Identity<T> Id { get; set; }
    }
}