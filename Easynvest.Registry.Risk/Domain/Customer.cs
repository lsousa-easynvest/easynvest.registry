﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Risk.Domain
{
    public class Customer: EntityWithId<string>, ICustomer
    {
        public Customer(decimal operationalLimit)
        {
            OperationalLimit = operationalLimit;
        }

        public decimal OperationalLimit { get; internal set; }
    }
}
