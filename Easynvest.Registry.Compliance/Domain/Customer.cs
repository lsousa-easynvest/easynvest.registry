﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Compliance.Domain
{
    public class Customer: EntityWithId<string>, ICustomer
    {
        public Customer(bool relatedParty, bool exposedPerson, PatrimonialSituation patrimonialSituation, Suitability suitability)
        {
            RelatedParty = relatedParty;
            ExposedPerson = exposedPerson;
            PatrimonialSituation = patrimonialSituation;
            Suitability = suitability;
        }

        public bool RelatedParty { get; internal set; }
        public bool ExposedPerson { get; internal set; }
        public bool SignedTerm { get; internal set; }
        public PatrimonialSituation PatrimonialSituation { get; internal set; }
        public Suitability Suitability { get; internal set; }
        
    }
}
