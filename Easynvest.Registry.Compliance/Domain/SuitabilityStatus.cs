﻿using System.ComponentModel;

namespace Easynvest.Registry.Compliance.Domain
{
    public enum SuitabilityStatus : byte
    {
        [Description("Ainda não preencheu")]
        NotFilledYet = 0,
        [Description("Preencheu")]
        Filled = 1,
        [Description("Refazer")]
        Remake = 2,
        [Description("Optou não responder")]
        Unanswered = 3
    }
}
