﻿using System.ComponentModel;

namespace Easynvest.Registry.Compliance.Domain
{
    public enum SuitabilityType : byte
    {
        [Description("Indefinido")]
        Undefined = 0,
        [Description("Conservador")]
        Conservative = 1,
        [Description("Moderado")]
        Moderate = 2,
        [Description("Experiente")]
        Experient = 3
    }
}
