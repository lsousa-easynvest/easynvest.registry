﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Compliance.Domain
{
    public class PatrimonialSituation : Aggregate
    {
        public PatrimonialSituation(decimal wage, decimal bankDepositAmount, decimal fixedIncomeAmount, decimal variableIncomeAmount)
        {
            Wage = wage;
            BankDepositAmount = bankDepositAmount;
            FixedIncomeAmount = fixedIncomeAmount;
            VariableIncomeAmount = variableIncomeAmount;
        }

        public decimal Wage { get; internal set; }
        public decimal BankDepositAmount { get; internal set; }
        public decimal FixedIncomeAmount { get; internal set; }
        public decimal VariableIncomeAmount { get; internal set; }
    }
}
