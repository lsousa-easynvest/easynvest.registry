﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Compliance.Domain
{
    public class Suitability : Aggregate
    {
        public Suitability(SuitabilityType type, SuitabilityStatus status, int scoreResult)
        {
            Type = type;
            Status = status;
            ScoreResult = scoreResult;
        }

        public SuitabilityType Type { get; internal set; }
        public SuitabilityStatus Status { get; internal set; }
        public int ScoreResult { get; internal set; }
    }
}
