﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Finance.Domain
{
    public class BankAccount : Aggregate
    {
        public BankAccount(Bank bank, string agencyNumber, int verifyingDigitAgency, int accountNumber, int verifyingDigitAccount, BankAccountType type, MaritalBankAccountType maritalBankAccountType, BankAccountOwner coOwner, bool mainBankAccount)
        {
            Bank = bank;
            AgencyNumber = agencyNumber;
            VerifyingDigitAgency = verifyingDigitAgency;
            AccountNumber = accountNumber;
            VerifyingDigitAccount = verifyingDigitAccount;
            Type = type;
            MaritalBankAccountType = maritalBankAccountType;
            CoOwner = coOwner;
            MainBankAccount = mainBankAccount;
        }

        public Bank Bank { get; internal set; }
        public string AgencyNumber { get; internal set; }
        public int VerifyingDigitAgency { get; internal set; }
        public int AccountNumber { get; internal set; }
        public int VerifyingDigitAccount { get; internal set; }
        public BankAccountType Type { get; internal set; }
        public MaritalBankAccountType MaritalBankAccountType { get; internal set; }
        public BankAccountOwner CoOwner { get; internal set; }
        public bool MainBankAccount { get; internal set; } = false;
    }
}
