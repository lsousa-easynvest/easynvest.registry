﻿using System.ComponentModel;

namespace Easynvest.Registry.Finance.Domain
{
    public enum BankAccountType : byte
    {
        [Description("Conta Corrente")]
        CurrentAccount = 0,

        [Description("Conta Poupança")]
        SavingsAccount = 1
    }
}