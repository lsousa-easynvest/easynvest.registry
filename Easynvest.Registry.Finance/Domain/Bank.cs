﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Finance.Domain
{
    public class Bank : ValueObject
    {
        public string Code { get; set; }
    }
}
