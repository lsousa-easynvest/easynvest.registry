﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Finance.Domain
{
    public class Account : Aggregate
    {
        public Account(AccountType accountType, bool isActive, int verifyingDigit)
        {
            AccountType = accountType;
            IsActive = isActive;
            VerifyingDigit = verifyingDigit;
        }

        public AccountType AccountType { get; internal set; }
        public bool IsActive { get; internal set; }
        public int VerifyingDigit { get; internal set; }

        public override string ToString()
        {
            return AccountType.ToString();
        }
    }
}
