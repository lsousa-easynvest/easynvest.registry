﻿using System.Collections.Generic;
using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Finance.Domain
{
    public class Customer: EntityWithId<string>, ICustomer
    {
        public Customer(IEnumerable<Account> accounts, IEnumerable<BankAccount> bankAccounts)
        {
            Accounts = accounts;
            BankAccounts = bankAccounts;
        }

        public IEnumerable<Account> Accounts { get; set; }
        public IEnumerable<BankAccount> BankAccounts { get; set; }
    }
}
