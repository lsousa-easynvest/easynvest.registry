﻿using System.ComponentModel;

namespace Easynvest.Registry.Finance.Domain
{
    public enum MaritalBankAccountType : byte
    {
        [Description("Conta Individual")]
        IndividualAccount = 0,

        [Description("Conta Conjunta")]
        JointAccount = 1
    }
}