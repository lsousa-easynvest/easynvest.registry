﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Finance.Domain
{
    public class BankAccountOwner : Aggregate,ICustomer
    {
        public Identity<string> Id { get; set; }

        public string Name { get; set; }
    }
}
