﻿using System.ComponentModel;

namespace Easynvest.Registry.Finance.Domain
{
    public enum AccountType : byte
    {
        [Description("Conta Não Criada")]
        None = 0,
        [Description("Conta Depósito")]
        Deposit = 1,
        [Description("Conta Investimento")]
        Investiment = 2,
        [Description("Conta Margem")]
        Margin = 3,
        [Description("Conta Aluguel")]
        Rent = 4,
        [Description("Conta BMF")]
        BMF = 5,
        [Description("Conta Corretora")]
        Broker = 6,
        [Description("Conta Migração Pendente")]
        PendingMigration = 7
    }
}
