﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Marketing.Domain
{
    public class Customer: EntityWithId<string>, ICustomer
    {
        public Customer(string easynvestKnowledge, string originApplicationClientId)
        {
            EasynvestKnowledge = easynvestKnowledge;
            OriginApplicationClientId = originApplicationClientId;
        }

        public string EasynvestKnowledge { get; internal set; }
        public string OriginApplicationClientId { get; internal set; }
    }
}
