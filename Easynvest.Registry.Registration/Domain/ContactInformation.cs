﻿using System.Collections.Generic;
using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class ContactInformation : Aggregate
    {
        public ContactInformation(Email email, Address preferencialAddress, IEnumerable<Address> addresses, IEnumerable<Phone> phones)
        {
            Email = email;
            PreferencialAddress = preferencialAddress;
            Addresses = addresses;
            Phones = phones;
        }

        public Email Email { get; internal set; }
        public Address PreferencialAddress { get; internal set; }
        public IEnumerable<Address> Addresses { get; internal set; }
        public IEnumerable<Phone> Phones { get; set; }
    }
}
