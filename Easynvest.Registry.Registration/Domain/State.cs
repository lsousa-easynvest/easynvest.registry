﻿namespace Easynvest.Registry.Registration.Domain
{
    public class State
    {
        public string Code { get; internal set; }
        public Country Country { get; internal set; }
    }
}
