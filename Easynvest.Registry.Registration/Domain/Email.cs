﻿using System;
using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class Email : Aggregate
    {
        public Email(string mail)
        {
            Mail = mail;
        }

        private string _Mail;
        public string Mail
        {
            get
            {
                return _Mail;

            }
            internal set
            {
                if (string.IsNullOrEmpty((value)))
                    throw new ArgumentNullException();

                _Mail = value;
            }
        }
    }
}
