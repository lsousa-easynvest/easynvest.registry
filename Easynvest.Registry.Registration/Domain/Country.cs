﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class Country : ValueObject
    {
        public string Code { get; set; }
    }
}
