﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class FamilyInformation : Aggregate
    {
        protected FamilyInformation()
        {

        }

        public FamilyInformation(string spouseName, string fatherName, string motherName)
        {
            SpouseName = spouseName;
            FatherName = fatherName;
            MotherName = motherName;
        }

        public string SpouseName { get; internal set; }
        public string FatherName { get; internal set; }
        public string MotherName { get; internal set; }
    }
}
