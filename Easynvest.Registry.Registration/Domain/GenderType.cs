﻿using System.ComponentModel;

namespace Easynvest.Registry.Registration.Domain
{
    public enum GenderType : byte
    {
        [Description("Masculino")]
        Male = 0,

        [Description("Feminino")]
        Female = 1
    }
}