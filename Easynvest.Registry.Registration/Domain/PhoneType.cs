﻿using System.ComponentModel;

namespace Easynvest.Registry.Registration.Domain
{
    public enum PhoneType : byte
    {
        [Description("Residencial")]
        Home = 0,

        [Description("Comercial")]
        Commercial = 1,

        [Description("Telefone Celular")]
        Cellphone = 2
    }
}