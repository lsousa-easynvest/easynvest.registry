﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class Address : Aggregate
    {
        public Address(string number, string postalCode, string streetName, string complement, string neighborhood, City city)
        {
            Number = number;
            PostalCode = postalCode;
            StreetName = streetName;
            Complement = complement;
            Neighborhood = neighborhood;
            City = city;
        }

        public string Number { get; internal set; }
        public string PostalCode { get; internal set; }
        public string StreetName { get; internal set; }
        public string Complement { get; internal set; }
        public string Neighborhood { get; internal set; }
        public City City { get; internal set; }
        
    }
}
