﻿using System;
using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class Customer : EntityWithId<string>, ICustomer
    {
        public Customer(string name, string nickname, DateTime birthDate, GenderType gender, ContactInformation personalInformation, ContactInformation businessInformation, FamilyInformation familyInformation)
        {
            Name = name;
            Nickname = nickname;
            BirthDate = birthDate;
            Gender = gender;
            PersonalInformation = personalInformation;
            BusinessInformation = businessInformation;
            FamilyInformation = familyInformation;
        }

        public string Name { get; internal set; }
        public string Nickname { get; internal set; }
        public Country Nationality { get; internal set; }
        public DateTime BirthDate { get; internal set; }
        public GenderType Gender { get; internal set; }
        public ContactInformation PersonalInformation { get; internal set; }
        public ContactInformation BusinessInformation { get; internal set; }
        public FamilyInformation FamilyInformation { get; internal set; }

        public void ChangeFamilyInformation(FamilyInformation familyInformation)
        {
            FamilyInformation = familyInformation;
        }
    }
}
