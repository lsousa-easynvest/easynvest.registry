﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class City : ValueObject
    {
        public string Code { get; internal set; }
        public State State { get; internal set; }
    }
}
