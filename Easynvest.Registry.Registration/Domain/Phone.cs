﻿using Easynvest.Registry.Shared.Domain;

namespace Easynvest.Registry.Registration.Domain
{
    public class Phone : Aggregate
    {
        public Phone(PhoneType type, int countryCode, int areaCode, long number)
        {
            Type = type;
            CountryCode = countryCode;
            AreaCode = areaCode;
            Number = number;
        }

        public PhoneType Type { get; internal set; }
        public int CountryCode { get; internal set; }
        public int AreaCode { get; internal set; }
        public long Number { get; internal set; }
    }
}
