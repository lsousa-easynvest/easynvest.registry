﻿using System;
using Easynvest.Registry.Registration.Commands;
using Easynvest.Registry.Shared;

namespace Easynvest.Registry.Registration.Handlers
{
    public class CustomerHandler : IHandler<RegisterNewCustomerCommand>, IHandler<ChangeCustomerNameCommand>
    {
        public void Handle(RegisterNewCustomerCommand command)
        {
            throw new NotImplementedException();
        }

        public void Handle(ChangeCustomerNameCommand command)
        {
            throw new NotImplementedException();
        }
    }
}
