﻿using Easynvest.Registry.Shared.Event;

namespace Easynvest.Registry.Registration.Events
{
    public sealed class AddressChanged : TransientEvent
    {
    }
}
